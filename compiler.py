from os import system
from sys import argv

from opcodes import *
from preprocessor import *
from syntax_check import *
from optimizer import *

SYNTAX_CHECK = True
PRINT_OPCODES = True
OPTIMIZE = True
DEBUG = True

HEIGHT = 100
WIDTH = 100
SCREEN_SIZE = HEIGHT * WIDTH

MAX_FPS = 100

def fancy_print(col1, col2, col3, col4=""):
    w1, w2, w3 = 7, 15, 50
    txt1 = (str(col1) + " " * w1)[:w1]
    txt2 = (str(col2) + " " * w2)[:w2]
    txt3 = "" if col3 == None else str(col3)
    txt3 = (txt3 + " "*w3)[:w3]
    print(txt1 + txt2 + txt3 + col4)

def opcode_print(i, op):
    fancy_print(str(i),  code_to_str(op.code), op.arg, chr(op.arg) if (isinstance(op.arg, int) and 0 <= op.arg) else "")
    
def print_opcodes(opcodes):
    opcodes = list(opcodes)
    for i in range(len(opcodes)):
        opcode_print(i, opcodes[i])

lines = read_file(argv[1])
lines = add_imports(lines)                          # Handles imports
lines = explicit_aliases(lines)                     # Turns the abbreviation "$name meaning" into "alias name meaning endalias"

words = get_words(lines)                            # Transforms lines into words (+strips comments and parses strings correctly)
words = expand_nxt(words)                           # Replaces nxt and nxt:n by fixed values and expands >a, <a, >a:n, <a:n.
words = expand_numerical_strings(words)             # Replaces numerical strings (like 7 or -32) by fixed values
words = expand_aliases(words)

opcodes = get_opcodes(words)                        # Transforms words into opcodes

if SYNTAX_CHECK:
    opcodes = list(opcodes)
    syntax_check(opcodes)                           # Matches if-endif, while-do-endwhile, proc-end
    opcodes = iter(opcodes)

opcodes = convert_ifwhile(opcodes)                      # Convert if/while into combinations of label/goto/gotoifzero

if OPTIMIZE:
    opcodes = list(opcodes)
    opcodes = simplify(opcodes)

opcodes, labels = extract_labels(opcodes)           # Labels are removed and the corresponding locations in code are stored
opcodes = rewrite_gotos(opcodes, labels)            # Labels are transformed into positions after goto/gotoifzero
if OPTIMIZE:
    opcodes = optimize_tail_recursion(opcodes)
    opcodes = optimize_gotos(opcodes)
    opcodes = optimize_tail_recursion(iter(opcodes))

opcodes = list(opcodes)

output = """#include <iostream>
#include <vector>
#include <stack>
#include <SDL2/SDL.h>
#define TICKS_PER_FRAME (1000 / 120)

using namespace std;

bool quit;

stack<int> mainstack;
stack<int> auxstack;
stack<int> sauxstack;
stack<void*> callstack;     // Stores label references
vector<int> rgstr_pos;      // Positively indexed elements of register
vector<int> rgstr_neg;      // Negatively indexed elements of register
vector<int> pressed_keys;

Uint32 ticks;
bool sdl_initialized;
SDL_Window* window;
SDL_Renderer* renderer;
SDL_Event event;

int pop(stack<int> * stack) {
    int a = stack->top();
    stack->pop();
    return a;
}

void* popv(stack<void*> * stack) {
    void* a = stack->top();
    stack->pop();
    return a;
}

void swap() {
    int a = pop(&mainstack);
    int b = pop(&mainstack);
    mainstack.push(a);
    mainstack.push(b);
}

void move(stack<int> * stack1, stack<int> * stack2) {
    stack2->push(pop(stack1));
}

void setat(vector<int> * vec, int index, int value) {
    if (index >= vec->size()) vec->resize(index+1, 0);
    (*vec)[index] = value;
}

int getat(vector<int> * vec, int index) {
    if (index >= vec->size()) return 0;
    return (*vec)[index];
}

void tostr() {
    string t = to_string(pop(&mainstack));
    for (int i = t.length() - 1 ; i >= 0; --i) {
        mainstack.push(t.at(i));
    }
    mainstack.push(t.length());
}

void handlekeys() {
    while(SDL_PollEvent(&event)){
        switch(event.type){
            case SDL_KEYDOWN:
                setat(&pressed_keys, event.key.keysym.sym, 1);
                break;
            case SDL_KEYUP:
                setat(&pressed_keys, event.key.keysym.sym, 0);
                break;
            case SDL_QUIT:
                quit = 1;
                break;
            default:
                break;
        }
    }
}

int keypressed(int key) {
    handlekeys();
    return getat(&pressed_keys, key);
}

void draw() {
    if (!sdl_initialized) {
        window = SDL_CreateWindow
        (
            "STACC Program", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            1000,
            1000,
            SDL_WINDOW_SHOWN
        );

        renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED);
        ticks = SDL_GetTicks();
        SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
        SDL_RenderClear( renderer );
        sdl_initialized = 1;
    }
    SDL_Rect r;
    r.w = 10;
    r.h = 10;
    SDL_RenderClear( renderer );
    for (int x=0; x<100; x++) {
        for (int y=0; y<100; y++) {
            r.x = 10*x;
            r.y = 10*y;
            int index = 100 * y + x;
            int value = 0;
            if (index < rgstr_pos.size()) {
                value = rgstr_pos.at(index);
            }
            SDL_SetRenderDrawColor( renderer, value, value, value, 255 );
            SDL_RenderFillRect( renderer, &r );
        }
    }
    while (SDL_GetTicks() - ticks < TICKS_PER_FRAME) {
        SDL_Delay(1);
    }
    SDL_RenderPresent(renderer);
    ticks = SDL_GetTicks();
    handlekeys();
}

void clear() {
    for (int i = 0; i<10000; i++) setat(&rgstr_pos, i, 0);
}

int rmod(int a, int b) {
    return b%a;
}

void print() {
    for (int a = pop(&mainstack) ; a > 0; --a) cout << (char)pop(&mainstack);
}

int main() {

    int tmp;
    int tmp2;
    void* tmp3;                 // Tmp label reference

    sdl_initialized = 0;
    quit = 0;

"""

t = "\t\t"
t2 = "\t\t\t"
t3 = "\t\t\t\t"

for i in range(len(opcodes)):
    op = opcodes[i]
    output += "\tlabel_" + str(i) + ": \t\t\t\t\t\t\t\t\t\t // [" + str(i) + "]\t\t" + code_to_str(op.code) + "\t\t" + (str(op.arg) if op.arg != None else "") + "\n"
    if op.code == NOP:
        output += t+ "__asm__(\"nop\\n\\t\");"
    elif op.code == PUSH:
        output += t+ "mainstack.push(" + str(op.arg) + ");\n"
    elif op.code == POP:
        output += t+ "mainstack.pop();\n"
    elif op.code == DUP:
        output += t+ "mainstack.push(mainstack.top());\n"
    elif op.code == SWAP:
        output += t+ "swap();\n"
    elif op.code == FROM:
        output += t+ "move(&auxstack, &mainstack);\n"
    elif op.code == TO:
        output += t+ "move(&mainstack, &auxstack);\n"
    elif op.code == SFROM:
        output += t+ "move(&sauxstack, &mainstack);\n"
    elif op.code == STO:
        output += t+ "move(&mainstack, &sauxstack);\n"
    elif op.code == GOTO:
        output += t+ "goto label_" + str(op.arg) +";\n"
    elif op.code == GOTOIFZERO:
        output += t+ "if (pop(&mainstack) == 0 ) goto label_" + str(op.arg) +";\n"
    elif op.code == CALL:
        output += t+ "callstack.push(&&label_" + str(i+1) + ");\n"
        output += t+ "goto label_" + str(op.arg) +";\n"
    elif op.code == RETURN:
        output += t+ "goto *popv(&callstack);\n"
    elif op.code == EXIT:
        output += t+ "goto quit;\n"
    elif op.code == ISPRESSED:
        output += t+ "mainstack.push(keypressed(pop(&mainstack)));\n"
    elif op.code == PRINT:
        output += t+ "print();\n"
    elif op.code == PAUSE:
        output += t+ "getc(stdin);\n"
    elif op.code == TOSTR:
        output += t+ "tostr();\n"
    elif op.code == NOT:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "mainstack.push((int)(tmp == 0));\n"
    elif op.code == EQ:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push((int)(tmp == tmp2));\n"
    elif op.code == GTR:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push((int)(tmp2 > tmp));\n"
    elif op.code == LSS:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push((int)(tmp2 < tmp));\n"
    elif op.code == ADD:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push(tmp + tmp2);\n"
    elif op.code == RSUB:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push(tmp - tmp2);\n"
    elif op.code == MUL:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push(tmp * tmp2);\n"
    elif op.code == RDIV:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push(tmp / tmp2);\n"
    elif op.code == MOD:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "tmp2 = pop(&mainstack);\n"
        output += t+ "mainstack.push(tmp2 % tmp);\n"
    elif op.code == GET:
        output += t+ "tmp = pop(&mainstack);\n"
        output += t+ "if (tmp >= 0) mainstack.push(getat(&rgstr_pos, tmp));\n"
        output += t+ "else mainstack.push(getat(&rgstr_neg, -tmp));\n"
    elif op.code == SET:
        output += t+ "tmp = pop(&mainstack);\n"            # the pointer
        output += t+ "if (tmp >= 0) setat(&rgstr_pos, tmp, pop(&mainstack));\n"
        output += t+ "else setat(&rgstr_neg, -tmp, pop(&mainstack));\n"

    elif op.code == DRAW:
        output += t+ "draw();\n"
        output += t+ "if (quit) goto quit;\n"
    elif op.code == CLEAR:
        output += t+ "clear();\n"
    elif op.code == SEESTACK:
        assert False
    elif op.code == RANDOM:
        output += t+ "mainstack.push(rand()%100);\n" 
    else:
        print("\t Not recognized:", op.code)

output += """

    quit:
        if(sdl_initialized) {
            SDL_DestroyWindow(window);
            SDL_Quit();
        }

        return EXIT_SUCCESS;

}"""

print(output)
