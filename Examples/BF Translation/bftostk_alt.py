# Brainfuck to STK, using registers as memory

#bf = input("Brainfuck code: ")
bf = open("mandelbrot.b", "r").readlines()[0]
f = open("result.stk", "w")

f.write("0 ")   # Initializing the needle position

def run_length(string):
    last_character = None
    count_last = 0
    constructed_list = []
    while string != "":
        h = string[0]
        string = string[1:]
        if last_character == None:
            last_character = h
            count_last = 1
        elif last_character == h:
            count_last += 1
        else:
            constructed_list.append((last_character, count_last))
            count_last = 1
            last_character = h
    if last_character == None:
        return []
    else:
        return constructed_list

bf = run_length(bf)

indent=0
tab = " "*4
newlined = False

for char, nb in bf:
    if char == "+":
        f.write("dup dup get " + str(nb) + " add swap set ")
        newlined = False
    elif char == "-":
        f.write("dup dup get " + str(-nb) + " add swap set ")
        newlined = False
    elif char == "<":
        f.write(str(-nb) + " add ")
        newlined = False
    elif char == ">":
        f.write(str(nb) + " add ")
        newlined = False
    elif char == ".":
        f.write("dup get " + str(nb) + " print ")
        newlined = False
    elif char == "[":
        for i in range(nb):
            if indent == 0:
                f.write("\n")
            if not newlined:
                f.write("\n" + tab * indent)
            f.write("while dup get do" + "\n" + tab * (indent+1))
            newlined = True
            indent += 1
    elif char == "]":
        for i in range(nb):
            if not newlined:
                f.write("\n" + tab * (indent-1))
            f.write("endwhile" + "\n" + tab * (indent-1))
            newlined = True
            indent -= 1
            if indent == 0:
                f.write("\n")

f.write("\n\nnop\n")

if indent != 0:
    print("Syntax error: cannot match all ['s with ]'s.")

f.close()
