# Brainfuck to STK, using stacks as memory

#bf = input("Brainfuck code: ")
bf = open("fractal.b", "r").readlines()[0]
f = open("result.stk", "w")

# Initialize the stacks
f.write("0 " * 100)
f.write("\n")
f.write("0 to " * 500)
f.write("\n\n")

indent=0
tab = " "*4
old_newlined = False

for char in bf:
    newlined = False
    if char == "+":
        f.write("1 add ")
    elif char == "-":
        f.write("-1 add ")
    elif char == "<":
        f.write("to ")
    elif char == ">":
        f.write("from ")
    elif char == ".":
        f.write("dup 1 print ")
    elif char == "[":
        if indent == 0:
            f.write("\n")
        if not old_newlined:
            f.write("\n" + tab * indent)
        f.write("while dup do" + "\n" + tab * (indent+1))
        newlined = True
        indent += 1
    elif char == "]":
        if not old_newlined:
            f.write("\n" + tab * (indent-1))
        f.write("endwhile" + "\n" + tab * (indent-1))
        newlined = True
        indent -= 1
        if indent == 0:
            f.write("\n")
    old_newlined = newlined

f.write("\n\nnop\n")

if indent != 0:
    print("Syntax error: cannot match all ['s with ]'s.")

f.close()
