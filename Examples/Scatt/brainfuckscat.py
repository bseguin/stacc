# Brainfuck to STK, using stacks as memory

#bf = input("Brainfuck code: ")
bf = open("mandelbrot.b", "r").readlines()[0]
f = open("result.stk", "w")

f.write("@scatt.stk\n\n")

# Initialize the stacks
f.write("nah dip deh " * 500)
f.write("\n\n")


for char in bf:
    if char == "+":
        f.write("doo wap ")
    elif char == "-":
        f.write("adun wap ")
    elif char == "<":
        f.write("deh ")
    elif char == ">":
        f.write("bee ")
    elif char == ".":
        f.write("dip doo wedeh ")
    elif char == "[":
        f.write("wah dip shoo ")
    elif char == "]":
        f.write("deewah ")

f.write("bah")

f.close()
