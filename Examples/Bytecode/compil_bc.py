from os import system
from sys import argv

from opcodes import *
from preprocessor import *
from syntax_check import *
from optimizer import *

SYNTAX_CHECK = True
PRINT_OPCODES = True
OPTIMIZE = True

def fancy_print(col1, col2, col3, col4=""):
    w1, w2, w3 = 5, 15, 50
    txt1 = (str(col1) + " " * w1)[:w1]
    txt2 = (str(col2) + " " * w2)[:w2]
    txt3 = "" if col3 == None else str(col3)
    txt3 = (txt3 + " "*w3)[:w3]
    print(txt1 + txt2 + txt3 + col4)

def opcode_print(i, op):
    fancy_print(str(i),  code_to_str(op.code), op.arg, chr(op.arg) if (isinstance(op.arg, int) and 0 <= op.arg) else "")

lines = read_file(argv[1])
lines = add_imports(lines)                          # Handles imports
lines = explicit_aliases(lines)                     # Turns the abbreviation "$name meaning" into "alias name meaning endalias"

words = get_words(lines)                            # Transforms lines into words (+strips comments and parses strings correctly)
words = expand_nxt(words)                           # Replaces nxt and nxt:n by fixed values and expands >a, <a, >a:n, <a:n.
words = expand_numerical_strings(words)             # Replaces numerical strings (like 7 or -32) by fixed values
words = expand_aliases(words)

opcodes = get_opcodes(words)                        # Transforms words into opcodes

if SYNTAX_CHECK:
    opcodes = list(opcodes)
    syntax_check(opcodes)                           # Matches if-endif, while-do-endwhile, proc-end
    opcodes = iter(opcodes)

opcodes = convert_ifwhile(opcodes)                      # Convert if/while into combinations of label/goto/gotoifzero

if OPTIMIZE:
    opcodes = list(opcodes)
    opcodes = simplify(opcodes)

opcodes, labels = extract_labels(opcodes)           # Labels are removed and the corresponding locations in code are stored
opcodes = rewrite_gotos(opcodes, labels)            # Labels are transformed into positions after goto/gotoifzero
if OPTIMIZE:
    opcodes = optimize_tail_recursion(opcodes)
    opcodes = optimize_gotos(opcodes)

opcodes = list(opcodes)

if PRINT_OPCODES:
    for i in range(len(opcodes)):
        opcode_print(i, opcodes[i])

bytecode = []
for op in opcodes:
    bytecode.append(op.code)
    if op.arg != None:
        u = op.arg + 32768
        bytecode.append(u // 256)
        bytecode.append(u % 256)
f = open("bytecode", "wb")
f.write(bytes(bytecode))
f.close()
