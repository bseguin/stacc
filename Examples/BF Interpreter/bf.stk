@stdlib.stk

;; BRAINFUCK INTERPRETER IN STACC

; Main stack contains the code, starting from the cursor, followed by the needle position
; The second auxiliary stack will always contain the beginning of the code, backwards, before the cursor

proc rewind
    dup "[" pop ifneq
        dup "]" pop ifeq
            sfrom           ; grab the previous character
            rewind          ; come back to just after the corresponding [
            sfrom           ; grab one character before that
            rewind
        else
            sfrom           ; grab the previous character
            rewind
        endif
    endif
end

proc forward
    dup "]" pop ifneq
        dup "[" pop ifeq
            sto             ; next character
            forward         ; go to just before corresponding ]
            sto             ; set that ] aside
            forward
        else
            sto             ; next character
            forward
        endif
    endif
end

"@" pop             ;eof

;Draw Sierpinski's triangle
"++++++++[>+>++++<<-]>++>>+<[-[>>+<<-]+>>]>+[-<<<[->[+[-]+>++>>>-<<]<[<]>>++++++[<<+++++>>-]+<<++.[-]<<]>.>+[>>]>+]" pop

;"+>>>>>>>++>+>+>+>++<[+[--[++>>--]->--[+[+<+[-<<+]++<<[-[->-[>>-]++<[<<]++<<-]+<<]>>>>-<<<<<++<-<<++++++[<++++++++>-]<.---<[->.[-]+++++>]>[[-]>>]]+>>--]+<+[-<+<+]++>>]<<<<[[<<]>>[-[+++<<-]+>>-]++[<<]<<<<<+>]>[->>[[>>>[>>]+[-[->>+>>>>-[-[+++<<[-]]+>>-]++[<<]]+<<]<-]<]]>>>>>>>]" pop

0                           ; initialize needle position

label mainloop
    swap                ; Put the next character on top of the stack

    dup "@" pop ifeq
        exit
    endif

    dup "+" pop ifeq
        sto             ; put the + in already read symbols
        dup inc         ; increment the value at the needle
        goto mainloop   ; go to next character
    endif
    
    dup "-" pop ifeq
        sto             ; put the - in already read symbols
        dup dec         ; increment the value at the needle
        goto mainloop   ; go to next character
    endif

    dup "<" pop ifeq
        sto             ; put the < in already read symbols
        1 sub           ; shift the needle to the left
        goto mainloop   ; go to next character
    endif

    dup ">" pop ifeq
        sto             ; put the > in already read symbols
        1 add           ; shift the needle to the right
        goto mainloop   ; go to next character
    endif

    dup "." pop ifeq
        sto             ; put the . in already read symbols
        dup get         ; get the value pointed by the needle
        1 print         ; turn it into a string and print it
        goto mainloop   ; go to next character
    endif

    dup "[" pop ifeq
        sto             ; put the [ in already read symbols
        dup get 0 ifeq
            to          ; store the needle position in the aux stack
            forward     ; go to just before corresponding ]
            sto         ; put the ] in already read symbols
            from        ; recover the needle position            
        endif
        goto mainloop
    endif

    dup "]" pop ifeq
        over get 0 ifeq
            sto         ; put the ] in already read symbols
        else
            swap to     ; store the needle position in the aux stack
            sfrom       ; grab the previous character
            rewind      ; come back to just after the corresponding [
            sto         ; put the [ in already read symbols
            from        ; recover the needle position
        endif
        goto mainloop
    endif

    "ERROR. " print
    1 "Unrecognized character: " list_concatenate_rev print "." print pause exit
