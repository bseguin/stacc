import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
from pygame.locals import *
from time import perf_counter, sleep

class Graphics:
    s = None
    width = None
    height = None
    pxlsize = None
    grid = None
    time_diff = 0
    old_time = 0
    
    def __init__(self, width, height, pxlsize=10, fps=15):
        self.width, self.height, self.pxlsize, self.time_diff = width, height, pxlsize, 1/fps
        self.old_time = perf_counter()

        pygame.init()
        self.s = pygame.display.set_mode((width * pxlsize, height * pxlsize))
        self.blank_grid()
    
    def blank_grid(self):
        self.grid = []
        for y in range(self.height):
            self.grid.append([0]*self.width)
        pygame.draw.rect(self.s, (0,0,0), pygame.Rect(0, 0, self.width*self.pxlsize, self.height*self.pxlsize),  0)        

    def draw_rect(self, x, y, value):
        if self.grid[y][x] != value:
            if value >= 0 and value <= 255:
                pygame.draw.rect(self.s, (value, value, value), pygame.Rect(x * self.pxlsize, y * self.pxlsize, self.pxlsize, self.pxlsize),  0)
                self.grid[y][x] = value

    def display(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
        elapsed = perf_counter() - self.old_time
        if elapsed <= self.time_diff:
            #sprint("FPS got slowed down from " + str(int(1/elapsed)))
            sleep(self.time_diff - elapsed)
        self.old_time = perf_counter()
        pygame.display.flip()

    def pressed_keys(self):
        return pygame.key.get_pressed()

    def is_pressed(self, ascii):
        return self.pressed_keys()[pygame.key.key_code(ascii)]
