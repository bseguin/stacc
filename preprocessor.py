from opcodes import *

class Line:
    t = None                    # Line content (text)
    w = None                    # Where does it appear? (file, line number)

    def __init__(self, w, t):
        self.w, self.t = w, t

class Word:
    t = None                    # The actual word (text or integer)
    w = None                    # Where does it appear? (file, line number)

    def __init__(self, w, t):
        self.w, self.t = w, t

def read_file(path):
    """ Reads lines from a .stk file. """
    cart_file = open(path, "r")
    lines = cart_file.readlines()
    cart_file.close()
    for l in range(len(lines)):
        yield Line("[" + path + ":" + str(l+1) + "]", lines[l])

def add_imports(lines, excluded_imports = []):
    """ Replaces imports invoked by '@' by the content of the file inside. Avoids loops. """
    for line in lines:
        simple_line = line.t.split(";")[0].strip()
        if len(simple_line) != 0 and simple_line[0] == "@":
            path = simple_line[1:]
            if path not in excluded_imports:
                excluded_imports.append(path)
                imported_lines = read_file(path)
                imported_lines = add_imports(imported_lines, excluded_imports)
                yield from imported_lines
        else:
            yield line

def explicit_aliases(lines):
    """ Replaces the abbreviation "$name meaning" by "alias name meaning endalias". """
    for line in lines:
        simple_line = line.t.strip()
        if len(simple_line) != 0 and simple_line[0] == "$":
            yield Line(line.w, "alias " + simple_line[1:])
            yield Line(line.w, "endalias")
        else:
            yield line

def decorate_words(w, words):
    """ Adds the line information to all words in a list of words (str). """
    for word in words:
        yield Word(w, word.lower())

def str_to_list(w, string):
    """ Transforms a string into the corresponding code. """
    n = len(string)
    string = list(string)
    while len(string) != 0:
        yield Word(w, ord(string.pop()))
    yield Word(w, n)

def get_words(lines):
    """ Trims comments, transforms the code into a list of words. """
    for line in lines:
        broken_line = line.t.split("\"")
        for i in range((len(broken_line) + 1)// 2):
            j = i*2
            line_chunk = broken_line[j]
            if ";" in line_chunk:
                line_chunk = line_chunk.split(";")[0].lower()
                yield from decorate_words(line.w, line_chunk.split())
                break
            else:
                yield from decorate_words(line.w, line_chunk.split())
            if j+1 < len(broken_line):
                yield from str_to_list(line.w, broken_line[j+1])

def expand_nxt(words):
    used_mem = 0

    for word in words:
        if isinstance(word.t, int):
            yield word
        elif word.t == "nxt":
            yield Word(word.w, used_mem)
            used_mem += 1
        elif word.t[:4] == "nxt:":
            N = int(word.t[4:])
            yield Word(word.w, used_mem)
            used_mem += N
        elif len(word.t) > 1 and word.t[0] in [">", "<"]:
            word_split = word.t[1:].split(":")
            k = None
            if len(word_split) > 1:
                k = word_split[1]
            ptr = word_split[0]
            yield Word(word.w, ptr)
            if k != None:
                yield Word(word.w, k)
                yield Word(word.w, "add")
            if word.t[0] == ">":
                yield Word(word.w, "set")
            else:
                yield Word(word.w, "get")
        else:
            yield word


def expand_numerical_strings(words):
    def is_numerical_string(value):
        if not isinstance(value, str):
            return False
        if len(value) == 0:
            return False
        elif len(value) > 1 and value[0] == "-":
            value = value[1:]
        for char in value:
            if char not in "0123456789":
                return False
        return True
    for word in words:
        if is_numerical_string(word.t):
            yield Word(word.w, int(word.t))
        else:
            yield word

def expand_aliases(words):
    def redecorate_words(w, words):
        """ Changes the line information of all words in a list of words (Word). """
        for word in words:
            yield Word(w + " > " + word.w, word.t)
    # This needs to be done after expansion of nxt to avoid multiplication of nxt's
    current = 0
    aliases = {}
    current_alias = None

    for word in words:
        if word.t == "alias":
            alias_name = next(words).t
            current_alias = alias_name
            aliases[alias_name] = []
        elif word.t == "endalias":
            current_alias = None
        elif current_alias != None:
            if word.t in aliases and word.t != current_alias:
                aliases[current_alias] += aliases[word.t]
            else:
                aliases[current_alias].append(word)
        else:
            if word.t in aliases:
                yield from redecorate_words(word.w, aliases[word.t])
            else:
                yield word

def get_opcodes(words):
    """ Generates opcodes from words. """
    procs = []
    for word in words:
        w = word.w
        if isinstance(word.t, int):
            yield Op(w, PUSH, "", word.t)
        elif word.t == "label":
            label = next(words).t
            yield Op(w, LABEL, "", label)
        elif word.t == "goto":
            label = next(words).t
            yield Op(w, GOTO_LBL, "", label)
        elif word.t == "gotoifzero":
            label = next(words).t
            yield Op(w, GOTOIFZERO_LBL, "", label)
        elif word.t == "proc":
            proc_name = next(words).t
            procs.append(proc_name)
            yield Op(w, PROC, "", proc_name)
        elif word.t in op_list:
            yield Op(w, op_list[word.t])
        elif word.t in procs:
            yield Op(w, CALL_PROC, word.t, word.t)
        else:
            assert False, "Unrecognized word: " + word.t + " (" + w + ")."

def convert_ifwhile(opcodes):    
    while_counter = 0
    while_stack = []

    if_counter = 0
    if_stack = []

    proc_counter = 0
    proc_stack = []

    procs = {}

    for op in opcodes:
        w = op.w
        if op.code == IF:
            label = "__if_" + str(if_counter)
            if_stack.append((label, 0))
            if_counter += 1
            yield Op(w, GOTOIFZERO_LBL, "", label + "_el0")

        elif op.code == ELSE:
            label, elcount = if_stack.pop()
            if_stack.append((label, elcount+1))
            yield Op(w, GOTO_LBL, "", label + "_end")
            yield Op(w, LABEL, "", label + "_el" + str(elcount))

        elif op.code == ELIF:
            label, elcount = if_stack[-1]
            yield Op(w, GOTOIFZERO_LBL, "", label + "_el" + str(elcount))

        elif op.code == ENDIF:
            label, elcount = if_stack.pop()
            yield Op(w, LABEL, "", label + "_el" + str(elcount))
            yield Op(w, LABEL, "", label + "_end")

        elif op.code == WHILE:
            label = "__while_" + str(while_counter)
            while_stack.append(label)
            while_counter += 1
            yield Op(w, LABEL, "", label + "_i")

        elif op.code == DO:
            label = while_stack[-1]
            yield Op(w, GOTOIFZERO_LBL, "", label + "_e")

        elif op.code == ENDWHILE:
            label = while_stack.pop()
            yield Op(w, GOTO_LBL, "", label + "_i")
            yield Op(w, LABEL, "", label + "_e")

        elif op.code == PROC:
            label = "__proc_" + str(proc_counter)
            proc_counter += 1
            proc_stack.append(label)
            yield Op(w, GOTO_LBL, "", label + "_e")
            yield Op(w, LABEL, "", label + "_i")
            proc_name = op.arg
            procs[proc_name] = label

        elif op.code == END:
            label = proc_stack.pop()
            yield Op(w, RETURN)
            yield Op(w, LABEL, "", label + "_e")

        elif op.code == CALL_PROC:
            label = procs[op.arg]
            yield Op(w, CALL_LBL, op.desc, label + "_i")

        else:
            yield op

def extract_labels(opcodes):
    """ Reads labels, store them and remove them from code """
    # This function and rewrite_gotos need to be called last since expanding aliases changes word counts

    result = []
    labels = {}

    for op in opcodes:
        if op.code == LABEL:
            labels[op.arg] = len(result)
        else:
            result.append(op)

    return iter(result), labels

translate_goto = {GOTO_LBL : GOTO, GOTOIFZERO_LBL : GOTOIFZERO, CALL_LBL : CALL}

def rewrite_gotos(opcodes, labels):
    for op in opcodes:
        if op.code in translate_goto:
            label = op.arg
            yield Op(op.w, translate_goto[op.code], op.desc, labels[label])
        else:
            yield op
