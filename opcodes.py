NOP = 0

# Stack operations
PUSH = 1
POP = 2
DUP = 3
SWAP = 4
FROM = 5
TO = 6
SFROM = 7
STO = 8

# Execution flow
GOTO = 10
GOTOIFZERO = 11
CALL = 12
RETURN = 13
EXIT = 14

# I/O
ISPRESSED = 20
PRINT = 21
PAUSE = 22
TOSTR = 23

# Numerical/boolean operations & comparisons
NOT = 30
EQ = 31
GTR = 32
LSS = 33
ADD = 34
RSUB = 35
MUL = 36
RDIV = 37
MOD = 38

# Register & misc.
GET = 40
SET = 41
DRAW = 42
CLEAR = 43
SEESTACK = 44
RANDOM = 45

# References/interaction with the callstack
GOTO_REF = 100
TO_REF = 101
FROM_REF = 102

# Fake opcodes (for the preprocessor)
NXT = -8
INPUTALIAS = -9
LABEL = -10
GOTO_LBL = -11
GOTOIFZERO_LBL = -12
CALL_LBL = -13
CALL_PROC = -14
PROC = -16
END = -17
IF = -18
ELSE = -19
ENDIF = -20
WHILE = -21
DO = -22
ENDWHILE = -23
ELIF = -24
LABEL_REF = -25
PROC_REF = -26
HERE_REF = -27
CALL_REF = -28

op_list = {
    "nop"           : NOP,
    "push"          : PUSH,
    "pop"           : POP,
    "dup"           : DUP,
    "swap"          : SWAP,
    "from"          : FROM,
    "to"            : TO,
    "sfrom"         : SFROM,
    "sto"           : STO,
    "goto"          : GOTO,
    "gotoifzero"    : GOTOIFZERO,
    "call"          : CALL,
    "return"        : RETURN,
    "exit"          : EXIT,
    "ispressed"     : ISPRESSED,
    "print"         : PRINT,
    "pause"         : PAUSE,
    "tostr"         : TOSTR,
    "not"           : NOT,
    "eq"            : EQ,
    "gtr"           : GTR,
    "lss"           : LSS,
    "add"           : ADD,
    "rsub"          : RSUB,
    "mul"           : MUL,
    "rdiv"          : RDIV,
    "mod"           : MOD,
    "get"           : GET,
    "set"           : SET,
    "draw"          : DRAW,
    "clear"         : CLEAR,
    "seestack"      : SEESTACK,
    "random"        : RANDOM,
    "goto-ref"      : GOTO_REF,
    "to-ref"        : TO_REF,
    "from-ref"      : FROM_REF,

    "nxt"           : NXT,
    "input-alias"   : INPUTALIAS,
    "if"            : IF,
    "else"          : ELSE,
    "elif"          : ELIF,
    "endif"         : ENDIF,
    "proc"          : PROC,
    "end"           : END,
    "while"         : WHILE,
    "do"            : DO,
    "endwhile"      : ENDWHILE,
    "label"         : LABEL,
    "goto-lbl"      : GOTO_LBL,
    "gotoifzero-lbl": GOTOIFZERO_LBL,
    "call-lbl"      : CALL_LBL,
    "call-proc"     : CALL_PROC,
    "label-ref"     : LABEL_REF,
    "proc-ref"      : PROC_REF,
    "here-ref"      : HERE_REF,
    "call-ref"      : CALL_REF
}

def code_to_str(code):
    for op in op_list:
        if op_list[op] == code:
            return op.upper()
    return "?"

class Op:
    code = None
    desc = None
    arg = None
    w = None

    def __init__(self, w, code, desc="", arg=None):
        self.w, self.code, self.desc, self.arg = w, code, desc, arg

