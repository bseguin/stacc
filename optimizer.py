from opcodes import *

# The optimizer must transform correct code into equivalent correct code
# But it MAY make some incorrect code correct, e.g. "swap swap" when there is one element on the stack is simplified to nothing.
# This is by design and not considered an issue, as code should be correct anyway before getting optimized...

simplifiable = [
    [PUSH, POP],
    [DUP, POP],
    [FROM, TO],
    [TO, FROM],
    [SFROM, STO],
    [STO, SFROM],
    [SWAP, SWAP]
]

def is_sublist(list1, list2):
    k = len(list1)
    n = len(list2)
    for i in range(n-k):
        if list2[i:i+k] == list1:
            return i
    return -1

def simplify(opcodes):
    opcodes = list(opcodes)
    oplist = [op.code for op in opcodes]
    for simplification in simplifiable:
        i = is_sublist(simplification, oplist)
        if i != -1:
            k = len(simplification)
            opcodes = opcodes[:i] + opcodes[i+k:]
            return simplify(opcodes)
    return opcodes

#TODO: optimize CALL followed by GOTO and GOTO that points to CALL

def optimize_gotos(opcodes):
    """ Warning: can get caught in infinite loop in situations like "label a goto a" """
    opcodes = list(opcodes)
    for i in range(len(opcodes)):
        new_op = None
        op = opcodes[i]
        if op.code in [GOTO, GOTOIFZERO, CALL]:
            destination = op.arg
            if destination >= len(opcodes):
                new_op = Op(op.w, EXIT, op.desc)
            else:
                op_dest = opcodes[destination]
                if op_dest.code == GOTO:
                    new_op = Op(op.w, op.code, op.desc, op_dest.arg)
                elif op.code == GOTO and op_dest.code == GOTOIFZERO:
                    new_op = Op(op.w, GOTOIFZERO, op.desc, op_dest.arg)
                elif op.code == GOTO and op_dest.code == RETURN:
                    new_op = Op(op.w, RETURN, op.desc)
                elif op.code == GOTO and op_dest.code == EXIT:
                    new_op = Op(op.w, EXIT, op.desc)
            if new_op != None:
                return optimize_gotos(
                    opcodes[:i] + [new_op] + opcodes[i+1:]
                )
    return opcodes

def optimize_tail_recursion(opcodes):
    for op in opcodes:
        if op.code == CALL:
            following = next(opcodes)
            if following.code == RETURN:
                yield Op(op.w, GOTO, op.desc, op.arg)
                yield following
            else:
                yield op
                yield following
        else:
            yield op
