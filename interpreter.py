from os import system
from sys import argv

from opcodes import *
from preprocessor import *
from syntax_check import *
from optimizer import *

from random import randint
from graphics import *

SYNTAX_CHECK = True
PRINT_OPCODES = True
OPTIMIZE = True
DEBUG = True

HEIGHT = 100
WIDTH = 100
SCREEN_SIZE = HEIGHT * WIDTH

MAX_FPS = 100

def fancy_print(col1, col2, col3, col4=""):
    w1, w2, w3 = 7, 15, 50
    txt1 = (str(col1) + " " * w1)[:w1]
    txt2 = (str(col2) + " " * w2)[:w2]
    txt3 = "" if col3 == None else str(col3)
    txt3 = (txt3 + " "*w3)[:w3]
    print(txt1 + txt2 + txt3 + col4)

def opcode_print(i, op):
    fancy_print(str(i),  code_to_str(op.code), op.arg, chr(op.arg) if (isinstance(op.arg, int) and 0 <= op.arg) else "")
    
def print_opcodes(opcodes):
    opcodes = list(opcodes)
    for i in range(len(opcodes)):
        opcode_print(i, opcodes[i])

lines = read_file(argv[1])
lines = add_imports(lines)                          # Handles imports
lines = explicit_aliases(lines)                     # Turns the abbreviation "$name meaning" into "alias name meaning endalias"

words = get_words(lines)                            # Transforms lines into words (+strips comments and parses strings correctly)
words = expand_nxt(words)                           # Replaces nxt and nxt:n by fixed values and expands >a, <a, >a:n, <a:n.
words = expand_numerical_strings(words)             # Replaces numerical strings (like 7 or -32) by fixed values
words = expand_aliases(words)

opcodes = get_opcodes(words)                        # Transforms words into opcodes

if SYNTAX_CHECK:
    opcodes = list(opcodes)
    syntax_check(opcodes)                           # Matches if-endif, while-do-endwhile, proc-end
    opcodes = iter(opcodes)

opcodes = convert_ifwhile(opcodes)                      # Convert if/while into combinations of label/goto/gotoifzero

if OPTIMIZE:
    opcodes = list(opcodes)
    opcodes = simplify(opcodes)

opcodes, labels = extract_labels(opcodes)           # Labels are removed and the corresponding locations in code are stored
opcodes = rewrite_gotos(opcodes, labels)            # Labels are transformed into positions after goto/gotoifzero
if OPTIMIZE:
    opcodes = optimize_tail_recursion(opcodes)
    opcodes = optimize_gotos(opcodes)
    opcodes = optimize_tail_recursion(iter(opcodes))

opcodes = list(opcodes)

def display_screen(memory):
    global g
    
    if g == None:
        g = Graphics(WIDTH, HEIGHT, 10, MAX_FPS)
    for y in range(HEIGHT):
        for x in range(WIDTH):
            pxl = WIDTH * y + x
            value = memory.get(pxl)
            g.draw_rect(x, y, value)
    g.display()

def push_string(stack, string):
    string = list(string)
    n = len(string)
    while len(string) > 0:
        stack.append(ord(string.pop()))
    stack.append(n)

if PRINT_OPCODES:
    print_opcodes(opcodes)

class Memory:
    first = []
    second = {}
    
    def __init__(self):
        self.first = [0] * SCREEN_SIZE
        self.second = {}

    def set(self, ptr, value):
        if 0 <= ptr < SCREEN_SIZE:
            self.first[ptr] = value
        else:
            self.second[ptr] = value

    def get(self, ptr):
        if 0 <= ptr < SCREEN_SIZE:
            return self.first[ptr]
        else:
            if ptr in self.second:
                return self.second[ptr]
            else:
                return 0

    def clearfirst(self):
        self.first = [0] * SCREEN_SIZE

g = None
current = 0
running = True
stack = []
auxstack = []
sauxstack = []
callstack = []
if DEBUG:
    call_messages = []
memory = Memory()
used_mem = 0

while running:
    jumped = False
    
    if current >= len(opcodes):
        running = False
        break
        
    op = opcodes[current]
    
    # print(memory.first)
    #opcode_print(current, op)
    
    try:
        if op.code == NOP:
            pass
        elif op.code == PUSH:
            if isinstance(op.arg, str):
                push_string(stack, op.arg)
            elif isinstance(op.arg, int):
                stack.append(op.arg)
            else:
                assert False, "Weird type on stack"
        elif op.code == POP:
                stack.pop()
        elif op.code == DUP:
            stack.append(stack[-1])
        elif op.code == SWAP:
            head, head2 = stack.pop(), stack.pop()
            stack.append(head)
            stack.append(head2)
        elif op.code == FROM:
            stack.append(auxstack.pop())
        elif op.code == TO:
            auxstack.append(stack.pop())
        elif op.code == SFROM:
            stack.append(sauxstack.pop())
        elif op.code == STO:
            sauxstack.append(stack.pop())

        elif op.code == GOTO:
            current = op.arg
            jumped = True
        elif op.code == GOTOIFZERO:
            if stack.pop() == 0:
                current = op.arg
                jumped = True
        elif op.code == CALL:
            callstack.append(current+1)
            if DEBUG:
                call_messages.append("Called " + op.desc + " from " + op.w)
            current = op.arg
            jumped = True
        elif op.code == RETURN and len(callstack) >= 1:
            current = callstack.pop()
            if DEBUG:
                call_messages.pop()
            jumped = True
        elif op.code == EXIT:
            running = False

        elif op.code == ISPRESSED:
            key = chr(stack.pop())
            stack.append(1 if g.is_pressed(key) else 0)
        elif op.code == PRINT:
            n = stack.pop()
            string = ""
            for i in range(n):
                string += chr(stack.pop())
            print(string, end="", flush=True)
        elif op.code == PAUSE:
            input()
        elif op.code == TOSTR:
            push_string(stack, str(stack.pop()))

        elif op.code == NOT:
            stack.append(int(stack.pop() == 0))
        elif op.code == EQ:
            stack.append(int(stack.pop() == stack.pop()))
        elif op.code == GTR:
            stack.append(int(stack.pop() < stack.pop()))
        elif op.code == LSS:
            stack.append(int(stack.pop() > stack.pop()))
        elif op.code == ADD:
            stack.append(stack.pop() + stack.pop())
        elif op.code == RSUB:
            stack.append(stack.pop() - stack.pop())
        elif op.code == MUL:
            stack.append(stack.pop()*stack.pop())
        elif op.code == RDIV:
            stack.append(stack.pop() // stack.pop())
        elif op.code == MOD:
            i1, i2 = stack.pop(), stack.pop()
            stack.append(i2 % i1)

        elif op.code == GET:
            ptr = stack.pop()
            stack.append(memory.get(ptr))
        elif op.code == SET:
            ptr, value = stack.pop(), stack.pop()
            memory.set(ptr, value)

        elif op.code == DRAW:
            display_screen(memory)
        elif op.code == CLEAR:
            memory.clearfirst()

        elif op.code == SEESTACK:
            print("Stack length :", len(stack)) # for debugging purposes
            print("Stack :", stack)
            print("Auxiliary stack :", auxstack)
            print("Second auxiliary stack :", sauxstack)

        elif op.code == RANDOM:
            stack.append(randint(1,100))
        else:
            print("\t Not recognized:", op.code)
        
        if not jumped:
            current += 1
    except IndexError:
        if DEBUG:
            print("Tried to pop from empty stack.")
            print("Faulty instruction is " + code_to_str(op.code) + " (" + op.w+ ")")
            print("State of stack:")
            print("\t Stack length :", len(stack)) # for debugging purposes
            print("\t Stack :", stack)
            print("\t Auxiliary stack :", auxstack)
            print("\t Second auxiliary stack :", sauxstack)
            print("State of callstack:")
            for message in call_messages:
                print("\t" + message)
        exit()
