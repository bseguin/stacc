from opcodes import *

def syntax_error_unexpected(unexpected_keyword, top_keyword=None):
    print("Syntax error")
    print("Unexpected " + code_to_str(unexpected_keyword.code) + " (" + unexpected_keyword.w + ")")
    if top_keyword != None:
        print("After unmatched " + code_to_str(top_keyword.code) + " (" + top_keyword.w + ")")
    exit()

def syntax_error_unmatched(keyword):
    print("Syntax error")
    print("Unmatched " + code_to_str(keyword.code) + " (" + keyword.w + ")")
    exit()

def syntax_check(opcodes):
    stack = []
    for op in opcodes:
        top = None if len(stack) == 0 else stack[-1]
        if op.code in [PROC, IF, WHILE]:
            stack.append(op)
        elif op.code == END:
            if top == None or top.code != PROC:
                syntax_error_unexpected(op, top)
            else:
                stack.pop()
        elif op.code == ELSE:
            if top == None or top.code != IF:
                syntax_error_unexpected(op, top)
        elif op.code == ENDIF:
            if top == None or top.code != IF:
                syntax_error_unexpected(op, top)
            else:
                stack.pop()
        elif op.code == DO:
            if top == None or top.code != WHILE:
                syntax_error_unexpected(op, top)
            else:
                stack.pop()
                stack.append(op)
        elif op.code == ENDWHILE:
            if top == None or top.code != DO:
                syntax_error_unexpected(op, top)
            else:
                stack.pop()

    if stack != []:
        for word in stack:
            syntax_error_unmatched(word)
