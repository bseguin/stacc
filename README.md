# STACC

On empile, on dépile, et on se désopile.

## Specification

```
;Ceci est un commentaire
@stdlib.cart        ; ceci importe la librairie standard
```

Le langage fonctionne avec quatre piles d'entiers (16bits signés, donc entre -32768 et 32767), dont l'une n'est pas directement accessible à l'utilisateur :
* La pile principale. On peut la manipuler avec des commandes "habituelles" telles que ```pop```, ```swap```, ```dup```. On peut empiler un entier (```-156```) ou une chaîne de caractères (```"Hello World"```) dessus ;
* Les deux piles auxiliaires. On peut dépiler le sommet de la pile principale et l'insérer à leur sommet avec ```to``` et ```sto``` respectivement, et faire l'opération inverse avec ```from``` et ```sfrom``` ;
* La pile d'appel. Elle est mise à jour automatiquement lorsqu'une procédure est appelée, et dépilée lorsqu'on atteint un ```return``` ou un ```end```.

Il y a de plus un registre, dont les adresses et les valeurs sont des entiers sur 16bits signés.

Pour manipuler la pile principale, on a les opérations mathématiques habituelles : ```not```, ```or```, ```and```, ```add```, ```sub```, ```mul```, ```div```, ```mod```. La philosophie est que toute opération consomme ses arguments, et les remplace par son résultat.

### Flot d'exécution

#### Alias :

```
$nom_d_alias liste_de_commandes

; est équivalent à :
alias nom_d_alias
    liste_de_commandes
endalias

nom_d_alias ; sera remplacé lors de la compilation par liste_de_commandes
```

Les alias ne peuvent pas être récursifs, mais ils peuvent employer d'autres alias définis précédemments.

#### Labels/goto:

```
label bonjour
    "Bonjour" print
    goto bonjour

; Provoque une boucle infinie
```

On peut utiliser ```goto``` avec un nom de label qui se trouve plus loin dans le programme.

#### Procédures

```
proc nom_procedure
    liste_de_commandes
end
```

On utilise ```return``` pour sortir de la procédure (```end``` provoque ```return``` automatiquement).

Les procédures sont appelées en écrivant leur nom. Il faut pour cela qu'elles aient déjà été définies.

Les procédures peuvent être récursives. La récursivité terminale est correctement implémentée.

#### Precalc (non implémenté)

```
precalc nom_precalc
    liste_d_instructions
endprecalc
```

Lorsqu'un ```precalc``` est défini, le compilateur récupère tout le code précédant le precalc (alias, procédures, etc.) suivi du code à l'intérieur du ```precalc``` (```liste_d_instructions```). Il utilise l'interpréteur pour exécuter le code en question et remplace, quand le programme est terminé, le bloc ```precalc``` par une procédure se chargeant de mettre directement sur les trois piles les valeurs qu'elles avaient à la fin de l'exécution, ainsi que le registre. Cela permet, par exemple, de générer des tables trigonométriques au moment de la compilation, etc.

#### Conditions:

```
[condition] if
    liste_de_commandes
endif
```

ou encore:

```
[condition] if
    liste_de_commandes_1
else
    liste_de_commandes_2
endif
```

Ici, ```[condition]``` désigne une commande ou liste de commandes quelconque qui conduit à ce qu'un entier soit empilé au sommet de la pile principale. ```if``` permet de faire quelque chose uniquement lorsque cette valeur est non nulle. Notez que ```if``` dépile la valeur.

La librairie standard offre des raccourcis tels que ```ifeq```, ```ifneq```, ```ifgtr```, ```iflss```, ```ifgeq``` et ```ifleq```.

#### Boucles

```
while [condition] do
    liste_de_commandes
endwhile
```

De même que ```if```, ```do``` dépile la valeur au sommet de la pile.

#### Opérations spéciales

* ```exit``` quitte le programme
* ```pause``` demande à l'utilisateur d'appuyer sur Entrée pour poursuivre l'exécution.

### Registres

Les commandes ```get``` et ```set``` (Syntaxe: ```ptr get```, ```valeur ptr set```).

Les abréviations ```<ptr```, ```<ptr:n```, ```>ptr```, ```>ptr:n```.

Les mots-clés ```nxt``` et ```nxt:n```.

### Entrée/sortie et déboguage

#### Entrée/sortie simple

Les commandes ```print```, ```tostr``` et ```random```.

#### Primitives graphiques

L'écran correspond aux positions 0-9999 de la mémoire, sur un écran 100x100.

Les commandes ```draw```, ```clear```, ```ispressed```.

#### Déboguage

La commandes ```seestack```.

La seule erreur qui peut avoir lieu à l'exécution survient lorsque le programme tente de dépiler depuis une pile vide. Dans ce cas, un débogueur tente de retracer pour l'utilisateur l'endroit problématique dans le code.

### Fonctions de la librairie standard

Listes (éléments puis longueur), nombres flottants (deux entiers : mantisse et exposant).

## Implémentation

* Préprocesseur : transforme le programme en une suite d'opcodes appartenant à un sous-ensemble minimal du langage ;
* Vérification syntaxique (peut être désactivée) : vérifie que les blocs proc-end, if-(else-)endif et while-do-endwhile sont bien formés ;
* Optimisation (peut être désactivée) : tente de simplifier le programme final ;
* Interpréteur : une "machine virtuelle" très simple lisant les opcodes et exécutant le programme.

Bytecode.
